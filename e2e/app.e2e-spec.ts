import { BusInCarparkPage } from './app.po';

describe('bus-in-carpark App', () => {
  let page: BusInCarparkPage;

  beforeEach(() => {
    page = new BusInCarparkPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
