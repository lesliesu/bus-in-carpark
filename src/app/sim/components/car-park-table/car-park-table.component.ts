import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {SimState} from '../../reducers/commands.reducers';
import {Store} from '@ngrx/store';
import {SpaceMatrixCliDataService} from '../../services/space-matrix-cli-data.service';
import {ParkFacing} from '../../models/park.postion';
import {Actions} from '@ngrx/effects';
import {CmdSuccessActions, CommandActions, CommandActionTypes, LeftRotateBus, MoveActions} from '../../actions/commands.actions';

@Component({
  selector: 'app-car-park-table',
  templateUrl: './car-park-table.component.html',
  styleUrls: ['./car-park-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CarParkTableComponent {
  parkFacing = ParkFacing;

  constructor(private store: Store<SimState>, public spaceMatrixService: SpaceMatrixCliDataService,
              actions$: Actions, private ref: ChangeDetectorRef) {
    actions$.ofType<CmdSuccessActions>(CommandActionTypes.PlaceSuccess,
      CommandActionTypes.MoveSuccess,
      CommandActionTypes.LeftSuccess,
      CommandActionTypes.RightSuccess).subscribe((action) => {
      this.ref.detectChanges();
    });
  }

}
