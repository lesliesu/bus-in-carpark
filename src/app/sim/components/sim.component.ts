import {Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {SimState} from '../reducers/commands.reducers';
import {PlaceBus} from '../actions/commands.actions';
import {ParkFacing} from '../models/park.postion';

@Component({
  selector: 'app-sim',
  templateUrl: './sim.component.html',
  styleUrls: ['./sim.component.css']
})
export class SimComponent {

  constructor(private store: Store<SimState>) {
  }

}
