import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {xPosMax, yPosMax} from '../../services/space-matrix-cli-data.service';
import {Store} from '@ngrx/store';
import {SimState} from '../../reducers/commands.reducers';
import {ParkFacing} from '../../models/park.postion';
import {
  InvalidCmd, LeftRotateBus, MoveBus, PlaceBus, Report, RightRotateBus,
  SimError
} from '../../actions/commands.actions';
import {Bus} from '../../models/bus';
import {AppState} from '../../../app.reducers';
import 'rxjs/add/operator/take';

@Component({
  selector: 'app-command',
  templateUrl: './command.component.html',
  styleUrls: ['./command.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CommandComponent {
  placeCmd: string;

  constructor(private store: Store<AppState>) {
  }

  dispatchPlaceCmd() {
    try {
      const [cmd, params] = this.placeCmd.trim().toUpperCase().split(' ');
      // TODO validate params carefully
      if (cmd !== 'PLACE' || !params) {
        this.store.dispatch(new InvalidCmd(this.placeCmd));
        return;
      }
      const [xPos, yPos, dir] = params.split(',');
      const xPosNum = +xPos;
      const yPosNum = +yPos;
      if (xPosNum < 0 || xPosNum > xPosMax || yPosNum < 0 || yPosNum > yPosMax) {
        this.store.dispatch(new InvalidCmd(this.placeCmd));
        return;
      }
      let facing;
      switch (dir) {
        case 'NORTH':
          facing = ParkFacing.N;
          break;
        case 'EAST':
          facing = ParkFacing.E;
          break;
        case 'SOUTH':
          facing = ParkFacing.S;
          break;
        case 'WEST':
          facing = ParkFacing.W;
          break;
        default:
          this.store.dispatch(new InvalidCmd(this.placeCmd));
          return;
      }
      const param = {bus: {}, pos: {xPos: xPosNum, yPos: yPosNum, facing: facing}};
      this.store.dispatch(new PlaceBus(param));
    } catch (e) {
      console.error(e);
      this.store.dispatch(new SimError(null));
    }
  }

  dispatchOtherCmd(cmd: 'L' | 'R' | 'MOVE' | 'REPORT') {
    const bus$ = this.store.select('simulator', 'operatingBus');
    bus$.take(1).subscribe((bus) => {
      switch (cmd) {
        case 'L':
          this.store.dispatch(new LeftRotateBus(bus));
          return;
        case 'R':
          this.store.dispatch(new RightRotateBus(bus));
          return;
        case 'MOVE':
          this.store.dispatch(new MoveBus(bus));
          return;
        case 'REPORT':
          this.store.dispatch(new Report(bus));
      }
    });
  }
}
