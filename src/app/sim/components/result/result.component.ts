import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {Actions} from '@ngrx/effects';
import {AllActions, CommandActionTypes, Report} from '../../actions/commands.actions';
import {ParkFacing, ParkPosition} from '../../models/park.postion';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ResultComponent {
  result: string;

  constructor(actions$: Actions, private ref: ChangeDetectorRef) {
    this.result = '';
    actions$.ofType<AllActions>(CommandActionTypes.PlaceSuccess, CommandActionTypes.PlaceFailed,
      CommandActionTypes.MoveSuccess, CommandActionTypes.MoveFailed,
      CommandActionTypes.LeftSuccess, CommandActionTypes.RightSuccess,
      CommandActionTypes.SimError, CommandActionTypes.InvalidCmd,
      CommandActionTypes.Place, CommandActionTypes.Move,
      CommandActionTypes.Left, CommandActionTypes.Right,
      CommandActionTypes.Report
    ).subscribe((action) => {
      this.result += action.type + '<br>';
      if (action.type === CommandActionTypes.Report) {
        this.result += this.report(action.payload.pos);
      }
      this.ref.detectChanges();
    });
  }


  private report(pos: ParkPosition) {
    let facing;
    switch (pos.facing) {
      case ParkFacing.N:
        facing = 'North';
        break;
      case ParkFacing.E:
        facing = 'East';
        break;
      case ParkFacing.S:
        facing = 'South';
        break;
      case ParkFacing.W:
        facing = 'West';
        break;
    }
    return '[Report] ' + pos.xPos + ',' + pos.yPos + ',' + facing + '<br>';
  }

}
