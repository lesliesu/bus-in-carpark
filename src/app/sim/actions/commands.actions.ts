import {Action} from '@ngrx/store';
import {Bus} from '../models/bus';
import {ParkPosition} from '../models/park.postion';
import {PlaceCommandParams} from 'app/sim/models/command.params';

export enum CommandActionTypes {
  Place = '[Commands] Place Bus',
  PlaceSuccess = '[Result] Place Bus Success',
  PlaceFailed = '[Result] Place Bus Failed',
  Move = '[Commands] Move',
  MoveSuccess = '[Result] Move Success',
  MoveFailed = '[Result] Move Failed',
  Left = '[Commands] Left',
  LeftSuccess = '[Result] Left Success',
  Right = '[Commands] Right',
  RightSuccess = '[Result] Right Success',
  Report = '[Commands] Report',
  SimError = '[Error] UnknoError Occurred',
  InvalidCmd = '[Error] Invalid Command'
}


export class PlaceBus implements Action {
  readonly type = CommandActionTypes.Place;

  constructor(public payload: PlaceCommandParams) {}
}


export class PlaceBusSuccess implements Action {
  readonly type = CommandActionTypes.PlaceSuccess;

  constructor(public payload: Bus) {}
}

export class PlaceBusFailed implements Action {
  readonly type = CommandActionTypes.PlaceFailed;

  constructor(public payload: Bus) {}
}

export class MoveBus implements Action {
  readonly type = CommandActionTypes.Move;

  constructor(public payload: Bus) {}
}


export class MoveBusSuccess implements Action {
  readonly type = CommandActionTypes.MoveSuccess;

  constructor(public payload: Bus) {}
}

export class MoveBusFailed implements Action {
  readonly type = CommandActionTypes.MoveFailed;

  constructor(public payload: Bus) {}
}

export class LeftRotateBus implements Action {
  readonly type = CommandActionTypes.Left;

  constructor(public payload: Bus) {}
}

export class LeftRotateBusSuccess implements Action {
  readonly type = CommandActionTypes.LeftSuccess;

  constructor(public payload: Bus) {}
}

export class RightRotateBus implements Action {
  readonly type = CommandActionTypes.Right;

  constructor(public payload: Bus) {}
}

export class RightRotateBusSuccess implements Action {
  readonly type = CommandActionTypes.RightSuccess;

  constructor(public payload: Bus) {}
}

export class Report implements Action {
  readonly type = CommandActionTypes.Report;

  constructor(public payload: Bus) {}
}

export class SimError implements Action {
  readonly type = CommandActionTypes.SimError;

  constructor(public payload: Bus) {}
}

export class InvalidCmd implements Action {
  readonly type = CommandActionTypes.InvalidCmd;

  constructor(public payload: String) {}
}

export type MoveActions = MoveBus | MoveBusSuccess | MoveBusFailed;
export type LeftActions = LeftRotateBus | LeftRotateBusSuccess;
export type RightActions = RightRotateBus | RightRotateBusSuccess;
export type PlaceActions = PlaceBus | PlaceBusSuccess | PlaceBusFailed;
export type CommandActions = PlaceActions | MoveActions | LeftActions | RightActions | Report;
export type CmdSuccessActions = PlaceBusSuccess | MoveBusSuccess | LeftRotateBusSuccess | RightRotateBusSuccess;
export type ResultActions = CmdSuccessActions | MoveBusFailed | PlaceBusFailed | SimError | InvalidCmd;
export type AllActions = CommandActions | ResultActions;
