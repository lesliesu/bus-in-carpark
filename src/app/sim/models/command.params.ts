import {Bus} from './bus';
import {ParkPosition} from './park.postion';

export interface PlaceCommandParams {
  bus: Bus;
  pos: ParkPosition;
}
