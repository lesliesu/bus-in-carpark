export interface ParkPosition {
  xPos: number;
  yPos: number;
  facing:  ParkFacing;
}

export enum ParkFacing {
  N = 0,
  E = 1,
  S = 2,
  W = 3,
}
