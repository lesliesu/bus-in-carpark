import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SimComponent } from './components/sim.component';
import {RouterModule} from '@angular/router';
import {StoreModule} from '@ngrx/store';
import {reducers} from './reducers/index';
import {EffectsModule} from '@ngrx/effects';
import {CommandEffects} from './effects/commands.effects';
import {CommandsService} from './services/commands.service';
import { CarParkTableComponent } from './components/car-park-table/car-park-table.component';
import { CommandComponent } from './components/command/command.component';
import { ResultComponent } from './components/result/result.component';
import {SpaceMatrixCliDataService} from './services/space-matrix-cli-data.service';
import {FormsModule} from '@angular/forms';
import * as cmd from './reducers/commands.reducers';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild([
      { path: '', component: SimComponent },
    ]),

    /**
     * StoreModule.forFeature is used for composing state
     * from feature modules. These modules can be loaded
     * eagerly or lazily and will be dynamically added to
     * the existing state.
     */
    // StoreModule.forFeature('simulator', cmd.reducer),

    /**
     * Effects.forFeature is used to register effects
     * from feature modules. Effects can be loaded
     * eagerly or lazily and will be started immediately.
     *
     * All Effects will only be instantiated once regardless of
     * whether they are registered once or multiple times.
     */
    EffectsModule.forFeature([CommandEffects]),
  ],
  declarations: [SimComponent, CarParkTableComponent, CommandComponent, ResultComponent],
  providers: [SpaceMatrixCliDataService, CommandsService]
})
export class SimModule { }
