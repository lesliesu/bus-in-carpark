import {Bus} from '../models/bus';
import {createSelector, State} from '@ngrx/store';
import {AllActions, CommandActions, CommandActionTypes} from '../actions/commands.actions';

export interface SimState {
  operatingBus: Bus;
  busy: boolean;
}

function getInitialState(): SimState {
  return {operatingBus: null, busy: false};
}

// export const simState = (state: SimState) => state;
// export const operatingBus = createSelector(simState, (state) => state.sim);

export function reducer(state = getInitialState(), action: AllActions): SimState {
  switch (action.type) {
    case CommandActionTypes.Place: {
      return {...state, busy: true};
    }

    case CommandActionTypes.Move: {
      return {...state, busy: true};
    }
    case CommandActionTypes.Left: {
      return {...state, busy: true};
    }
    case CommandActionTypes.Right: {
      return {...state, busy: true};
    }

    case CommandActionTypes.PlaceSuccess: {
      return {
        operatingBus: action.payload,
        busy: false,
      };
    }

    case CommandActionTypes.PlaceFailed:
      return {
        operatingBus: state.operatingBus,
        busy: false,
      };

    case CommandActionTypes.MoveSuccess:
      return {
        operatingBus: action.payload,
        busy: false,
      };

    case CommandActionTypes.MoveFailed:
      return {
        operatingBus: state.operatingBus,
        busy: false,
      };

    case CommandActionTypes.LeftSuccess:
      return {
        operatingBus: action.payload,
        busy: false,
      };

    case CommandActionTypes.RightSuccess:
      return {
        operatingBus: action.payload,
        busy: false,
      };
    case CommandActionTypes.InvalidCmd:
      return {
        operatingBus: state.operatingBus,
        busy: false,
      };
    case CommandActionTypes.SimError:
      return {
        operatingBus: state.operatingBus,
        busy: false,
      };
    default:
      return state;
  }
}
