import {Injectable} from '@angular/core';
import {Actions, Effect} from '@ngrx/effects';
import {
  CommandActionTypes,
  LeftRotateBus,
  LeftRotateBusSuccess, MoveBus, MoveBusSuccess,
  PlaceBus,
  PlaceBusFailed,
  PlaceBusSuccess,
  RightRotateBus,
  RightRotateBusSuccess, SimError, MoveBusFailed
} from '../actions/commands.actions';
import 'rxjs/add/operator/switchMap';
import {CommandsService} from '../services/commands.service';
import {catchError, map} from 'rxjs/operators';
import {of} from 'rxjs/observable/of';

@Injectable()
export class CommandEffects {
  constructor(private cmdService: CommandsService, private actions$: Actions) {
  }

  // tslint:disable-next-line:member-ordering
  @Effect() placeBus$ = this.actions$.ofType<PlaceBus>(CommandActionTypes.Place)
    .switchMap((action) => this.cmdService.placeBus(action.payload)
      .pipe(map((bus) => {
          if (bus) {
            return new PlaceBusSuccess(bus);
          } else {
            return new PlaceBusFailed(action.payload.bus);
          }
        }),
        catchError((bus) => of(new SimError(bus))))
    );

  // tslint:disable-next-line:member-ordering
  @Effect() leftRotateBus$ = this.actions$.ofType<LeftRotateBus>(CommandActionTypes.Left)
    .switchMap((action) => this.cmdService.rotate(action.payload, 'L')
      .pipe(map((bus) => {
          return new LeftRotateBusSuccess(bus);
        }),
        catchError((bus) => of(new SimError(bus))))
    );

  // tslint:disable-next-line:member-ordering
  @Effect() rightRotateBus$ = this.actions$.ofType<RightRotateBus>(CommandActionTypes.Right)
    .switchMap((action) => this.cmdService.rotate(action.payload, 'R')
      .pipe(map((bus) => {
          return new RightRotateBusSuccess(bus);
        }),
        catchError((bus) => of(new SimError(bus))))
    );

  // tslint:disable-next-line:member-ordering
  @Effect() moveBus$ = this.actions$.ofType<MoveBus>(CommandActionTypes.Move)
    .switchMap((action) => this.cmdService.move(action.payload)
      .pipe(map((bus) => {
          if (bus) {
            return new MoveBusSuccess(bus);
          } else {
            return new MoveBusFailed(action.payload);
          }
        }),
        catchError((bus) => of(new SimError(bus))))
    );
}
