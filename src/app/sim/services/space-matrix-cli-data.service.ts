import { Injectable } from '@angular/core';
import {Bus} from '../models/bus';
import {isNullOrUndefined} from 'util';
import {ParkPosition} from '../models/park.postion';

export const xPosMax = 5;
export const yPosMax = 5;

@Injectable()
export class SpaceMatrixCliDataService {
  private spaceMatrix: Bus[][];

  constructor() {
    this.spaceMatrix = [];
    for (let i = 0; i < xPosMax; i++) {
      const column = [];
      for (let j = 0; j < yPosMax; j++) {
        column.push(null);
      }
      this.spaceMatrix.push(column);
    }
  }

  get parkSpaces(): Bus[][] {
    return this.spaceMatrix;
  }

  posToMatrixIndex(pos: ParkPosition): { xIndex: number, yIndex: number } {
    const xIndex = Math.abs(pos.yPos - yPosMax + 1) % yPosMax;
    const yIndex = pos.xPos;
    return {xIndex, yIndex};
  }

  isValidParkPosition(pos: ParkPosition): boolean {
    if (!pos) {
      return false;
    }
    const [x, y] = [pos.xPos, pos.yPos];
    const {xIndex, yIndex} = this.posToMatrixIndex(pos);
    return !isNullOrUndefined(x) &&
      !isNullOrUndefined(y) &&
      x >= 0 && x < xPosMax &&
      y >= 0 && y < yPosMax &&
      !this.spaceMatrix[xIndex][yIndex];
  }

}
