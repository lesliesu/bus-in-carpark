import { TestBed, inject } from '@angular/core/testing';

import {SpaceMatrixCliDataService, xPosMax, yPosMax} from './space-matrix-cli-data.service';
import {ParkFacing} from '../models/park.postion';

describe('SpaceMatrixCliDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SpaceMatrixCliDataService]
    });
  });

  it('should initialise park space properly', inject([SpaceMatrixCliDataService], (service: SpaceMatrixCliDataService) => {
    expect(service).toBeTruthy();
    expect(service.parkSpaces.length).toEqual(xPosMax);
    expect(service.parkSpaces[0].length).toEqual(yPosMax);
  }));

  it ('posToMatrixIndex function works', inject([SpaceMatrixCliDataService], (service: SpaceMatrixCliDataService) => {
    expect(service.posToMatrixIndex({xPos: 1, yPos: 4, facing: ParkFacing.N})).toEqual({xIndex: 0, yIndex: 1});
    expect(service.posToMatrixIndex({xPos: 2, yPos: 3, facing: ParkFacing.N})).toEqual({xIndex: 1, yIndex: 2});
  }));
});
