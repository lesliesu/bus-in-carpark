import { TestBed, inject } from '@angular/core/testing';

import { CommandsService } from './commands.service';
import {SpaceMatrixCliDataService} from './space-matrix-cli-data.service';
import {ParkFacing} from '../models/park.postion';

describe('CommandsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CommandsService, SpaceMatrixCliDataService]
    });
  });

  it('placeBus function works', inject([CommandsService], (service: CommandsService) => {
    expect(service).toBeTruthy();
    const params = {bus: {}, pos: {xPos: 1, yPos: 4, facing: ParkFacing.W}};
    service.placeBus(params).subscribe((bus) => {
      expect(bus.pos).toBeDefined();
      expect(bus.pos.xPos).toEqual(1);
    });
  }));
});
