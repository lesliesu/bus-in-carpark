import {Injectable} from '@angular/core';
import {Bus} from '../models/bus';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/delay';
import 'rxjs/add/observable/of';
import {ParkFacing, ParkPosition} from '../models/park.postion';
import {PlaceCommandParams} from '../models/command.params';
import {SpaceMatrixCliDataService, yPosMax} from './space-matrix-cli-data.service';

@Injectable()
export class CommandsService {

  static isSameParkPosition(pos1: ParkPosition, pos2: ParkPosition) {
    return pos1.xPos === pos2.xPos && pos1.yPos === pos2.yPos && pos1.facing === pos2.facing;
  }

  constructor(private spaceMatrixService: SpaceMatrixCliDataService) {
  }

  // Usually change of state will need call server side api to update database in real application.
  // We simply delay 1 seconds and change the payload data at client side here for this simulator
  placeBus(params: PlaceCommandParams): Observable<Bus> {
    if (this.spaceMatrixService.isValidParkPosition(params.pos)) {
      const bus = {...params.bus, pos: params.pos};
      this.updateSpaceMatrix(bus);
      return Observable.of(bus).delay(1000);
    } else {
      return Observable.of(null).delay(1000);
    }
  }

  move(bus: Bus): Observable<Bus> {
    let pos = {...bus.pos};
    const targetPos = this.getMovePosition(bus.pos);
    if (this.spaceMatrixService.isValidParkPosition(targetPos) && !CommandsService.isSameParkPosition(targetPos, bus.pos)) {
      const {xIndex, yIndex} = this.spaceMatrixService.posToMatrixIndex(pos);
      this.spaceMatrixService.parkSpaces[xIndex][yIndex] = null;
      pos = targetPos;
      const _bus = {...bus, pos: pos};
      this.updateSpaceMatrix(_bus);
      return Observable.of(_bus).delay(1000);
    } else {
      return Observable.of(null).delay(1000);
    }
  }


  rotate(bus: Bus, dir: 'L' | 'R') {
    const pos = {...bus.pos};
    const _bus = {...bus, pos: pos};
    if (dir === 'L') { // LEFT
      pos.facing = (bus.pos.facing + 4 - 1) % 4; // 4 directions, move left 90 deg
    } else {          // RIGHT
      pos.facing = (bus.pos.facing + 1) % 4;     // 4 directions, move right 90 deg
    }
    this.updateSpaceMatrix(_bus);
    return Observable.of(_bus).delay(1000);
  }

  private updateSpaceMatrix(_bus: Bus) {
    const {xIndex, yIndex} = this.spaceMatrixService.posToMatrixIndex(_bus.pos);
    this.spaceMatrixService.parkSpaces[xIndex][yIndex] = _bus;
  }

  private getMovePosition(currentPos: ParkPosition): ParkPosition {
    let [x, y] = [currentPos.xPos, currentPos.yPos];

    switch (currentPos.facing) {
      case ParkFacing.N:
        y++;
        break;
      case ParkFacing.S:
        y--;
        break;
      case ParkFacing.E:
        x++;
        break;
      case ParkFacing.W:
        x--;
        break;
      default:
        throw new Error('Unsupported direction');
    }
    return {...currentPos, xPos: x, yPos: y};
  }


}
