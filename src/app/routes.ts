import { Routes } from '@angular/router';

export const routes: Routes = [
  { path: '', redirectTo: '/sim', pathMatch: 'full' },
  {
    path: 'sim',
    loadChildren: './sim/sim.module#SimModule'
  }
];
