# BusInCarpark
A simulator of bus in car park using Angular 4 and ngrx (Redux)

![DEMO image](https://bitbucket.org/repo/EgRr4ez/images/3895514440-Capture%20(23).PNG)

- demo imgae url: https://bitbucket.org/repo/EgRr4ez/images/3895514440-Capture%20(23).PNG

## Install
- Run `npm install -g @angular/cli` to install Angular Cli
- Run `npm install` to install all dependencies. (It will take a while.)

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Notes
- Press Enter key to execute Place command
- For simple and performance, park space matrix is not in Redux store, it is a client side shared two dimention array that intialise once the application start.
- A few unit test cases for services have been added. (To add more tests specs for all services' functions)
